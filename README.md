# OpenPowerlifting Data Service

This repository exports the latest [opl-data](https://gitlab.com/openpowerlifting/opl-data) into one large csv file every night.

View the data service at <https://openpowerlifting.gitlab.io/opl-csv/>.
