# OpenPowerlifting Data Service

### All Powerlifting Data, a Click Away!

OpenPowerlifting is a community service project to create a permanent, open archive of the world's powerlifting data.
In support of this mission, all of our data and code are available for download in convenient formats.
There is no need to scrape our websites: you can download everything here.

Work on the OpenPowerlifting project is conducted in the open with a visible and permanent data modification history.
If you are interested in contributing to the project, the best way is through our [GitLab repository](https://gitlab.com/openpowerlifting/opl-data).

If you would like to support our effort, please consider [donating to the project](https://www.patreon.com/openpowerlifting).

### Data Licensing

All competition data available on this website are contributed to the Public Domain.

The OpenPowerlifting database contains facts that, in and of themselves, are not protected by copyright law.
However, the copyright laws of some jurisdictions may cover database design and structure.

To the extent possible under law, all competition data on this website are waived of all copyright and related or neighboring rights. The work is published from the United States.

Although you are under no requirement to do so, if you incorporate OpenPowerlifting data into your project, please consider adding a statement of attribution, so that people may know about this project and help contribute data.

#### Sample Attribution Text
```
This page uses data from the OpenPowerlifting project, https://www.openpowerlifting.org.
You may download a copy of the data at https://data.openpowerlifting.org.
```

If you modify the data or add useful new data, please consider contributing the changes back so the entire powerlifting community may benefit.
