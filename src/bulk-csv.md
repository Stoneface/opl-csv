# Bulk CSV Downloads

The files below contain all columns in a single spreadsheet for convenience: no `JOIN` needed.

- Updated: {{#include ../scratch/mdvars/authored-date}}.
- Revision: [{{#include ../scratch/mdvars/commit-short}}](https://gitlab.com/openpowerlifting/opl-data/-/commits/{{#include ../scratch/mdvars/commit}}).

New versions are published nightly.

File | Size | Rows | Description
:-- | --: | --: | :--
[openpowerlifting-latest.zip](https://openpowerlifting.gitlab.io/opl-csv/files/openpowerlifting-latest.zip) | {{#include ../scratch/mdvars/openpowerlifting-size}} | {{#include ../scratch/mdvars/openpowerlifting-linecount}} | The complete dataset.
[openipf-latest.zip](https://openpowerlifting.gitlab.io/opl-csv/files/openipf-latest.zip) | {{#include ../scratch/mdvars/openipf-size}} | {{#include ../scratch/mdvars/openipf-linecount}} | Data from all IPF affiliates.
